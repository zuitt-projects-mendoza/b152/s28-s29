/*
	First, we load the expressjs module into our application and saved it in
	a variable called express.
*/
const express = require("express");
/*
	Create an application with expressjs
	This creates an application that uses express and stores it as app
	app is our server
*/
const app = express();

//port is jsut a variable to contian the port number we want to designate to our server
const port = 4000;

//express.json() is a method from express which allow us to handle the streaming of data and automatically parse the incoming JSON from our request's body.
//app.use() is used to run a method or another function for our expressjs api.
app.use(express.json());

//mock data
let users = [

	{
		username: "tinaRCBC",
		email:"tinaLRCBC@gmail.com",
		password: "tinaRCBC990"
	},
	{
		username: "gwenstacy1",
		email: "spidergwen@gmail.com",
		password: "gwenOGspider"
	}


];

let items = [

	{
		name: "Stick-O",
		price: 70,
		isActive: true
	},
	{
		name:"Doritos",
		price: 150,
		isActive: true
	}

];


//app.listen() allows us to designated the server to run on the indicated port number and run console log afterwards.

//Express has a methods/function to use as routes corresponding to each HTTP method.
//app.get(<endpoint>,<function handling request and response)

app.get('/',(req,res)=>{

	//Once the route is accessed, we can send a response with the use of res.send()
	//res.send() actually combines our writeHead() and end().
	//It is used to send a response to the client and ends the response.
	res.send("Hello World from our first ExpressJS API!");

})

/*
	Mini-Activity

	Create a GET route in ExpressJS which will be able to send a message in the client:

	//endpoint: '/hello'

	Message: "Hello from Batch 152!"

	Create a get method request in postman
		-add the correct URL and method
		-send the request
		-take a screenshot of the result/response and send it in the hangouts.
*/

app.get('/hello',(req,res)=>{

	res.send('Hello from Batch 152');

})

app.get('/users',(req,res)=>{

	//res.send() already stringifies for you.
	res.send(users);

})

//How do we get data from the client as a request body?
app.post('/users',(req,res)=>{

	//With the use of express.json(), we simply have to access the body property of our request object to get the body/input of the request.
	//The receiving of data and the parsing of JSON to JS object has already been done by express.json()

	//Note: When dealing with a route that receives data from a request body is a good practice to check the incoming data from the client.
	console.log(req.body);

	//simulate creating a new user document
	let newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	}

	//push newUser into the users array
	users.push(newUser);
	console.log(users);

	//send the updated the users array in the client
	res.send(users);

})


//delete user route
app.delete('/users',(req,res)=>{

	users.pop();
	console.log(users);

	res.send(users)

})

//update user route
app.put('/users/:index',(req,res)=>{

	//Updating our user will require us to add an input from our client
	//req.body will contain the password.
	console.log(req.body);

	//req.params object which contains the value of the url params.
	//url params is captured by route parameter (:parameterName) and saved as a property in req.params
	console.log(req.params);

	//parseInt the value of the number coming from req.params
	let index = parseInt(req.params.index);

	//get the item we want to update with our index number from our URL params
	users[index].password = req.body.password;

	//send the updated user to the client.
	//provide the index variable to be the index for the particular item in the array.
	res.send(users[index])


/*	//get the index from your request body first
	users[req.body.index].password = req.body.password

	//send the updated user to client
	res.send(users[req.body.index])*/

})

//get details of single user
//GET method requests should not have a request body. It may have headers for additional information. We can pass small amount of data somewhere else: Through the url.
//Route params are values we can pass via the URL.
//This is done specifically to allow us to send small amount of data into our server through the request URL.
//Route parameters can be defined in the endpoint of a route with :parameterName
app.get('/users/getSingleUser/:index',(req,res)=>{

	//URL: http://localhost:4000/users/getSingleUser/0
	//req.params is an object that contains the values passed as route params.
	//In req.params, the parameter name given in the route endpoint becomes the property name in
	//Our URL is a string.
	console.log(req.params);
	/*
		{ index: '0'}
	*/

	//parseInt() the value of req.params.index to convert the value from string to number.
	let index = parseInt(req.params.index);
	console.log(index);

	//send the user with the appropriate index number
	res.send(users[index])

})

/*

Activity:

Same Endpoint: /items

Create a new route to get and send the items array in the client.

Create a new route to create and add a new item object in the items array.
	-Send the updated items array in the client.
	-Check the post method route for our users for reference.

Create a new route which can update the price of a single item in the array.
	-Pass the index number of the item you want to update in the request body.
	-Add the index number of the item you want to update to access it and its price property.
	-Re-assign the new price from our request body.
	-Send the updated item in the client. 

*/

//items routes

app.get('/items',(req,res)=>{

	res.send(items);

})

app.post('/items',(req,res)=>{

	console.log(req.body);
	//contains the details/values passed from the request body in postman.

	//creating a new document/object to push/add into our array:
	let newItem = {

		name: req.body.name,
		price: req.body.price,
		isActive: req.body.isActive

	}

	//push/add the new item in the array
	items.push(newItem);
	console.log(items);

	//send the updated array in the client.
	res.send(items);

})

/*
	Mini-Activity:
	
	Update/Re-factor the route for updating item price. Instead of passing the index number from the request body, pass the index number from the request URL using route params.
		-send the updated item in the client.

	-Send a screenshot of your code in the hangouts.

*/

//price update route.
app.put('/items/:index',(req,res)=>{

	console.log(req.body);
	console.log(req.params);

	let index = parseInt(req.params.index);

	//update the price property of our selected item with the value from our request body.

	items[index].price = req.body.price;

	res.send(items[index]);


/*	//how can we update the property of an object in an array?
	//items[0].price
	items[req.body.index].price = req.body.price

	//send the updated item in the client.
	res.send(items[req.body.index])*/
})

/*
	Activity:

	Note: Add items first.

	Create a new route with '/items/getSingleItem/:index' endpoint. This route should allow us to GET the details of a single item.
		-Pass the index number of the item you want to get via the url as url params.
		-get the data from the url params.
		-send the particular item from our array using its index to the client.

	Create a new route to update an item with '/items/archive/:index' endpoint.	This route should allow us to UPDATE the isActive property of our product.
		-Pass the index number of the item you want to de-activate via the url as url params.
		-Access the particular item with its index. Access the isActive property of the item and re-assign it to false.
		-send the update item in the client

	Create a new route to update an item with '/items/activate/:index' endpoint.	This route should allow us to UPDATE the isActive property of our product.
		-Pass the index number of the item you want to de-activate via the url as url params.
		-Access the particular item with its index. Access the isActive property of the item and re-assign it to true.
		-send the update item in the client

	Pushing Instructions

	Go to Gitbash:

		-go to your b152/s28-s29 folder.
		-add your updates to be committed: git add .
		-commit your changes to be pushed: git commit -m "includes expressJS Intro activity 2"
		-push your updates to your online repo: git push origin master

	Go to Boodle:
		-copy the url of the home page for your s28-s29 repo (URL on browser not the URL from clone button) and link it to boodle:

		WDC028-29 | ExpressJS - Intro

*/

app.get('/items/getSingleItem/:index',(req,res)=>{


	console.log(req.params);

	let index = parseInt(req.params.index);
	console.log(index);

	res.send(items[index])

})

app.put('/items/archive/:index',(req,res)=>{

	console.log(req.params);

	let index = parseInt(req.params.index);

	items[index].isActive = false;

	res.send(items[index]);

})

app.put('/items/activate/:index',(req,res)=>{

	console.log(req.params);

	let index = parseInt(req.params.index);

	items[index].isActive = true;

	res.send(items[index]);

})

app.listen(port,()=>console.log(`Server is running at port ${port}`));